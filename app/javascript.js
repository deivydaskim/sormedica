function initMap() {
    var baltic = {lat: 56.9996241, lng: 23.0113021};
    var vilnius = {lat: 54.6821375, lng: 25.2640614};
    var riga = {lat: 56.9471549, lng: 24.1019043};
    var talinas = {lat: 59.4364549, lng: 24.7547283};
    
    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 6,
        center: baltic,
        gestureHandling: 'none',
        zoomControl: false,
        disableDefaultUI: true,
        styles: [
            {
              "elementType": "geometry",
              "stylers": [
                {
                  "color": "#212121"
                }
              ]
            },
            {
              "elementType": "labels.icon",
              "stylers": [
                {
                  "visibility": "off"
                }
              ]
            },
            {
              "elementType": "labels.text.fill",
              "stylers": [
                {
                  "color": "#757575"
                }
              ]
            },
            {
              "elementType": "labels.text.stroke",
              "stylers": [
                {
                  "color": "#212121"
                }
              ]
            },
            {
              "featureType": "administrative",
              "elementType": "geometry",
              "stylers": [
                {
                  "color": "#757575"
                }
              ]
            },
            {
              "featureType": "administrative.country",
              "elementType": "labels.text.fill",
              "stylers": [
                {
                  "color": "#9e9e9e"
                }
              ]
            },
            {
              "featureType": "administrative.land_parcel",
              "stylers": [
                {
                  "visibility": "off"
                }
              ]
            },
            {
              "featureType": "administrative.locality",
              "elementType": "labels.text.fill",
              "stylers": [
                {
                  "color": "#bdbdbd"
                }
              ]
            },
            {
              "featureType": "poi",
              "elementType": "labels.text.fill",
              "stylers": [
                {
                  "color": "#757575"
                }
              ]
            },
            {
              "featureType": "poi.park",
              "elementType": "geometry",
              "stylers": [
                {
                  "color": "#181818"
                }
              ]
            },
            {
              "featureType": "poi.park",
              "elementType": "labels.text.fill",
              "stylers": [
                {
                  "color": "#616161"
                }
              ]
            },
            {
              "featureType": "poi.park",
              "elementType": "labels.text.stroke",
              "stylers": [
                {
                  "color": "#1b1b1b"
                }
              ]
            },
            {
              "featureType": "road",
              "elementType": "geometry.fill",
              "stylers": [
                {
                  "color": "#2c2c2c"
                }
              ]
            },
            {
              "featureType": "road",
              "elementType": "labels.text.fill",
              "stylers": [
                {
                  "color": "#8a8a8a"
                }
              ]
            },
            {
              "featureType": "road.arterial",
              "elementType": "geometry",
              "stylers": [
                {
                  "color": "#373737"
                }
              ]
            },
            {
              "featureType": "road.highway",
              "elementType": "geometry",
              "stylers": [
                {
                  "color": "#3c3c3c"
                }
              ]
            },
            {
              "featureType": "road.highway.controlled_access",
              "elementType": "geometry",
              "stylers": [
                {
                  "color": "#4e4e4e"
                }
              ]
            },
            {
              "featureType": "road.local",
              "elementType": "labels.text.fill",
              "stylers": [
                {
                  "color": "#616161"
                }
              ]
            },
            {
              "featureType": "transit",
              "elementType": "labels.text.fill",
              "stylers": [
                {
                  "color": "#757575"
                }
              ]
            },
            {
              "featureType": "water",
              "elementType": "geometry",
              "stylers": [
                {
                  "color": "#000000"
                }
              ]
            },
            {
              "featureType": "water",
              "elementType": "labels.text.fill",
              "stylers": [
                {
                  "color": "#3d3d3d"
                }
              ]
            }
          ]
    });
    
    var icon = 'images/marker.png';
    var image1 = 'images/textbox1.png';
    var marker = new google.maps.Marker({
        position: vilnius,
        icon: icon,
        map: map
    });
    var marker1 = new google.maps.Marker({
        position: vilnius,
        icon: icon,
        map: map
    });
    var marker2 = new google.maps.Marker({
        position: riga,
        icon: icon,
        map: map
    });
    var marker3 = new google.maps.Marker({
        position: talinas,
        icon: icon,
        map: map
    });
    google.maps.event.addListener(marker1, "mouseover", function() {
      clear();
      marker = new google.maps.Marker({
        position: vilnius,
        icon: image1,
        map: map
      });
      setTimeout(clear, 2000)
    });
    google.maps.event.addListener(marker2, "mouseover", function() {
      clear();
      marker = new google.maps.Marker({
        position: riga,
        icon: image1,
        map: map
      });
      setTimeout(clear, 2000)
    });
    google.maps.event.addListener(marker3, "mouseover", function() {
      clear();
      marker = new google.maps.Marker({
        position: talinas,
        icon: image1,
        map: map
      });
      setTimeout(clear, 2000)
    });

    function clear(){
      marker.setMap(null);
    }
    $("#vilnius").hover(
      function() {
        marker = new google.maps.Marker({
          position: vilnius,
          icon: image1,
          map: map
        });
      }, function() {
        marker.setMap(null);
      }
    );
    $("#riga").hover(
      function() {
        marker = new google.maps.Marker({
          position: riga,
          icon: image1,
          map: map
        });
      }, function() {
        marker.setMap(null);
      }
    );
    $("#talinas").hover(
      function() {
        marker = new google.maps.Marker({
          position: talinas,
          icon: image1,
          map: map
        });
      }, function() {
        marker.setMap(null);
      }
    );
}

var $hamburger = $(".hamburger");
var $navbg = $("#navbg");
var $menu = $("#menu");
var $lang = $(".lang");
var $body = $("body");
$hamburger.on("click", function(e) {
  $hamburger.toggleClass("is-active");
  $navbg.toggleClass("navbg");
  $menu.toggleClass("responsive");
  $body.toggleClass("overflow");
});


//Fixed images slider
var maintop = $('#main').offset().top;
var footertop = $('.remejai').offset().top;
var bottomtop = $('.bottom').offset().top;
var content1 = $('.content:nth-of-type(1)').offset().top;
var content2 = $('.content:nth-of-type(2)').offset().top;
var content3 = $('.content:nth-of-type(3)').offset().top;
var content4 = $('.content:nth-of-type(4)').offset().top;
var x = 0;
function fixDiv() {
  var $cache = $('aside');
  if ($(window).scrollTop() > maintop && $(window).scrollTop() < bottomtop)
    $cache.css({
      'position': 'fixed',
      'top': '0',
      'left': '0'
    });
  else
    $cache.css({
      'position': 'relative',
      'top': 'auto'
    });
    if ($(window).scrollTop() > content4)
    $(".top").css({
      'background-image': 'url("images/section4.jpg")'
    })
    else if ($(window).scrollTop() > content3)
    $(".top").css({
      'background-image': 'url("images/section3.jpg")'
    })
    else if ($(window).scrollTop() > content2)
    $(".top").css({
      'background-image': 'url("images/section2.jpg")'
    })
    else if ($(window).scrollTop() > content1)
    $(".top").css({
      'background-image': 'url("images/section1.jpg")'
    })
}
$(window).scroll(fixDiv);
fixDiv();

// Blogas sprendimas

// var content1value = true;
// var content2value = true;
// var content3value = true;
// var content4value = true;
// function content1Slider() {
//   if(content1value){
//   $('.top').fadeOut('normal', function(){
//     $('.top').fadeIn();
//     $(".top").css({
//       'background-image': 'url("images/section1.jpg")'
//     })
//   });
//   content1value = false;
//   content2value = true;
//   content3value = true;
//   content4value = true;
// }
// }
// function content2Slider() {
//   if(content2value){
//   $('.top').fadeOut('normal', function(){
//     $('.top').fadeIn();
//     $(".top").css({
//       'background-image': 'url("images/section2.jpg")'
//     })
//   });
//   content2value = false;
//   content1value = true;
//   content3value = true;
//   content4value = true;
// }
// }
// function content3Slider() {
//   if(content3value){
//   $('.top').fadeOut('normal', function(){
//     $('.top').fadeIn();
//     $(".top").css({
//       'background-image': 'url("images/section3.jpg")'
//     })
//   });
//   content3value = false;
//   content2value = true;
//   content1value = true;
//   content4value = true;
// }
// }
// function content4Slider() {
//   if(content4value){
//   $('.top').fadeOut('normal', function(){
//     $('.top').fadeIn();
//     $(".top").css({
//       'background-image': 'url("images/section4.jpg")'
//     })
//   });
//   content4value = false;
//   content2value = true;
//   content3value = true;
//   content1value = true;
// }
// }
